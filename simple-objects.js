/*
 * Simple Object Demo
 *
 * Concepts: object literal, properties, element access
 */

// Constructor ( "ctor" ) - builds and initializes an instance of a prototype
function Container() {
    this.isOpen = false;
    this.isLocked = true;
    this.contents = [];

    for (var i = 0; i < arguments.length; i++) {
        this.contents.push(arguments[i]);
    }
}

/** Returns a string representation of this container. */
Container.prototype.toString = function() {
    if (this.isOpen) {
        return "container with " + this.contents;
    } else if (this.isLocked) {
        return "locked container";
    } else {
        return "unopened container";
    }
}

/** Unlocks a container. */
Container.prototype.unlock = function() {
    this.isLocked = false;
}

/** Unlocks a container. */
function unlock(container) {
    container.isLocked = false;
}

/** Opens any unlocked container. */
Container.prototype.open = function() {
    if (this.isLocked === false) {
        this.isOpen = true;
    }
}

/** Opens any unlocked container. */
function open(container) {
    if (container.isLocked === false) {
        container.isOpen = true;
    }
}

/** Checks whether an open container holds anything. */
Container.prototype.isEmpty = function() {
    if (this.isOpen) {
        return (this.contents.length === 0);
    }
}

/** Checks whether an open container holds anything. */
function isEmpty(container) {
    return (container.contents.length === 0);
}

/** Adds an item to an open container. */
Container.prototype.put = function(item) {
    if (this.isOpen) {
        this.contents.push(item);
    }
}

/** Adds an item to an open container. */
function putIn(item, container) {
    if (container.isOpen) {
        container.contents.push(item);
    }
}

/** Removes and returns all items held by a container. */
Container.prototype.takeAll = function() {
    var itemsTaken;
    if (this.isOpen) {
        itemsTaken = this.contents;
        this.contents = [];
    }
    return itemsTaken;
    //return this.contents.slice(0, this.contents.length); // the easy way
}

/** Removes and returns all items held by a container. */
function takeAllFrom(container) {
    var itemsTaken;
    if (container.isOpen) {
        itemsTaken = container.contents;
        container.contents = [];
    }
    return itemsTaken;
    //return container.contents.slice(0, container.contents.length); // the easy way
}

// Create a few instances of the Container prototype and use them
var jar = new Container();
var treasureChest = new Container("gold", "magic sword", "crown of wisdom");
var vault = new Container("jewels", "microchip");
var lootBox = new Container("gold", "magic sword", "crown of wisdom");

putIn("cookies", jar);
jar.put("butterfly");

/** Compare two containers shallowly - are they the same object in memory? */
function shallowEquals(container1, container2) {
    return (container1 === container2);
}

console.log( "Chest = Vault? " + shallowEquals(treasureChest, vault) ); // false
console.log( "Chest = Loot Box? " + shallowEquals(treasureChest, lootBox) ); // true

/** Compare two containers down to a shallow comparison of their properties. */
function deepEquals(container1, container2) {
    return (container1.isOpen === container2.isOpen)
            && (container1.isLocked === container2.isLocked)
            && (container1.contents === container2.contents);
}

console.log( "Again Chest = Loot Box? " + deepEquals(treasureChest, lootBox) ); // false

/** Compare two arrays down to a shallow comparison of their elements. */
function arrayEquals(array1, array2) {
    var i;
    if (array1.length !== array2.length) {
        return false;
    }
    for (i = 0; i < array1.length; i++) {
        if (array1[i] !== array2[i]) {
            return false;
        }
    }
    return true;
}

/** Compare two containers down to their array elements. */
function deeperEquals(container1, container2) {
    return (container1.isOpen === container2.isOpen)
            && (container1.isLocked === container2.isLocked)
            && arrayEquals(container1.contents, container2.contents);
}

console.log( "Finally Chest = Loot Box? " + deeperEquals(treasureChest, lootBox) ); // true

