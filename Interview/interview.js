var currentFloor = 1; // start on the ground floor

// our first function - given a string, it displays it on the page
function showScene(descrip){
    document.getElementById("scene").innerHTML = descrip;
}

// another function - changes to the floor above if possible
function moveUp(){
    var message;
    // decide what message to show depending on which floor we are on
    if (currentFloor === 1) {
        currentFloor = 2; // change floor
        message = "You are now on the second floor.";
    } else if (currentFloor === 2) {
        currentFloor = 3; // change floor
        message = "You are now on the third floor.";
    } else {
        message = "You are already at the top.";
    }
    showScene(message);
}

// another function - changes to the floor below if possible
function moveDown(){
    var message;
    // decide what message to show depending on which floor we are on
    if (currentFloor === 3) {
        currentFloor = 2; // change floor
        message = "You are now on the second floor.";
    } else if (currentFloor === 2) {
        currentFloor = 1; // change floor
        message = "You are now on the first floor.";
    } else {
        message = "There is no basement in this building.";
    }
    showScene(message);
}

