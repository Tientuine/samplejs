/*
 * CMPT 120
 * Project #1
 * Matthew A Johnson
 * 09/22/15
 */

// Convert some lengths

alert( "Length Converter" );

var lengthIn = prompt( "Enter a length in inches: " );
var lengthCm = lengthIn * 2.54;

alert( lengthIn + " inches is equivalent to " + lengthCm + " centimeters." );

