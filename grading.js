/*
 * CMPT 120
 * Project #1
 * Matthew A Johnson
 * 09/22/15
 */

// Exam Scoring Assistant

alert("Exam Scoring Assistant");

var studentName = prompt("Please enter the student's name: ");
var numQuestions = prompt("How many questions are on the exam? ");
var numCorrect = prompt("How many questions did the student answer correctly? ");

var grade = numCorrect / numQuestions;

alert("The student earned a score of " + grade + " on the exam.");
alert("Goodbye!");

