/*
 * CMPT 120
 * Project #1
 * Matthew A Johnson
 * 09/22/15
 */

// Convert Fahrenheit to Celsius (Centigrade)

alert("Temperature Converter");

var tempF = prompt("Enter the temperature in Fahrenheit: ");
var tempC = (tempF - 32) / 1.8;

alert(tempF + " F is equivalent to " + tempC + " C ");

